package com.santander.sgt.code_analizer;

import java.util.List;

import com.santander.sgt.code_analizer.service.impl.FileAnalizerConsumer;
import com.santander.sgt.code_analizer.service.impl.RecursiveFilesReader;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args )
    {
    	if (args.length != 1) {
    		System.out.println("java -jar code-analizer <<path>>");
    	}
    	else {
	    	String path = args[0];
	    	
	    	RecursiveFilesReader reader = new RecursiveFilesReader();
	    	List<?> listOfFiles = reader.getAllFileNames(path);
	    	FileAnalizerConsumer analizer = new FileAnalizerConsumer();
	    	for (Object filePathObject:listOfFiles) {
	    		String filePath = (String)filePathObject;
	    		analizer.analize(filePath);
	    	}
    	}    	
    }
}
