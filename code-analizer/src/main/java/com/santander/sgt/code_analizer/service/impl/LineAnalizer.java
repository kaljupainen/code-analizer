package com.santander.sgt.code_analizer.service.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.function.Consumer;

import com.santander.sgt.code_analizer.beans.PatternsOfCode;
import com.santander.sgt.code_analizer.beans.ResponseOfParsing;
import com.santander.sgt.code_analizer.beans.impl.ClassLoadPattern;
import com.santander.sgt.code_analizer.beans.impl.SystemGetModule;

public class LineAnalizer implements Consumer<String>{

	private static ArrayList<PatternsOfCode> listOfPatters = null;
	
	Hashtable<String, Boolean> repeatedTable = new Hashtable<String, Boolean>();
	
	public static void initPatterns() {
		listOfPatters = new ArrayList<PatternsOfCode>();
		listOfPatters.add(new ClassLoadPattern());
		listOfPatters.add(new SystemGetModule());
	}
	
	@Override
	public void accept(String lineAndFile) {
		
		String file = lineAndFile.substring(0, lineAndFile.indexOf("~"));
		String line = lineAndFile.substring(lineAndFile.indexOf("~") + 1);
		
		if (listOfPatters == null) {
			LineAnalizer.initPatterns();
		}
		
		
		ArrayList<ResponseOfParsing> listOfResponses = new ArrayList<ResponseOfParsing>();
		for (PatternsOfCode pattern:listOfPatters) {
			if (pattern.compare(line)) {
				ResponseOfParsing resp = pattern.analizeLine(line);
				resp.setFileOfElement(file);
				listOfResponses.add(resp);
			}
		}
		
		/* Do anything with the responses */		
		for (ResponseOfParsing resp: listOfResponses) {
			String itemId = resp.getFileOfElement() + "," +
					resp.getPackageOfElement() + "," +
					resp.getNameOfElement();
				
			
			if (repeatedTable.get(itemId) == null) {
				System.out.println(itemId + "," +
						resp.getTypeOfElement());	
				
				repeatedTable.put(itemId, true);
			}					
		}
	}
}
