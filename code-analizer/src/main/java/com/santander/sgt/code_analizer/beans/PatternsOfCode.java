package com.santander.sgt.code_analizer.beans;

public abstract class PatternsOfCode {
	/* This class is the parent for a code pattern. A code pattern is
	 * a string that we search in the code and that, when we find,
	 * generates some kind of action.
	 * 
	 * Pattern should be in lower case, and use some special characters
	 * (actually it's a mask).
	 */
	
	private String pattern;	
	
	public abstract boolean compare(String text);
	public abstract ResponseOfParsing analizeLine(String text);
}
