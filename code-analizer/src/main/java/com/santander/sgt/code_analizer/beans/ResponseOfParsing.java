package com.santander.sgt.code_analizer.beans;

public class ResponseOfParsing {
	private String fileOfElement;
	private String packageOfElement;
	private String nameOfElement;
	private String typeOfElement;
	
	public String getFileOfElement() {
		return fileOfElement;
	}
	public void setFileOfElement(String fileOfElement) {
		this.fileOfElement = fileOfElement;
	}
	public String getPackageOfElement() {
		return packageOfElement;
	}
	public void setPackageOfElement(String packageOfElement) {
		this.packageOfElement = packageOfElement;
	}
	public String getNameOfElement() {
		return nameOfElement;
	}
	public void setNameOfElement(String nameOfElement) {
		this.nameOfElement = nameOfElement;
	}
	public String getTypeOfElement() {
		return typeOfElement;
	}
	public void setTypeOfElement(String typeOfElement) {
		this.typeOfElement = typeOfElement;
	}
}
