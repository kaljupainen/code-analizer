package com.santander.sgt.code_analizer.beans.impl;

import com.santander.sgt.code_analizer.beans.PatternsOfCode;
import com.santander.sgt.code_analizer.beans.ResponseOfParsing;

public class ClassLoadPattern extends PatternsOfCode {

	@Override
	public boolean compare(String text) {
		if (text.toLowerCase().indexOf("class.load") > -1) {
			return true;						
		}
		else {
			return false;
		}
	}

	@Override
	public ResponseOfParsing analizeLine(String text) {
		/* var EndOfProcessControllerClass = Class.load("com.c3.sdi.library.vmtools", "ItemStatusController");  */
		text = text.substring(text.indexOf("\"") + 1);
		String packageOfClass = text.substring(0, text.indexOf("\""));
		text = text.substring(text.indexOf("\"") + 1);
		
		String nameOfClass = text.substring(text.indexOf("\"") + 1, text.lastIndexOf('"'));
	
		ResponseOfParsing response = new ResponseOfParsing();
		response.setNameOfElement(nameOfClass);
		response.setPackageOfElement(packageOfClass);
		response.setTypeOfElement("class");
		
		return response;
	}

}
