package com.santander.sgt.code_analizer.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;

import javax.tools.FileObject;

public class FileAnalizerConsumer {
		
	public void analize(String filePath) {
		try {
			LineAnalizer lineAnal = new LineAnalizer();
			Files.lines(Path.of(filePath)).forEach(line -> lineAnal.accept(filePath + "~" + line));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
