package com.santander.sgt.code_analizer.beans.impl;

import com.santander.sgt.code_analizer.beans.PatternsOfCode;
import com.santander.sgt.code_analizer.beans.ResponseOfParsing;

public class SystemGetModule extends PatternsOfCode {

	@Override
	public boolean compare(String text) {
		if (text.toLowerCase().indexOf("system.getmodule") > -1) {
			return true;						
		}
		else {
			return false;
		}
	}

	@Override
	public ResponseOfParsing analizeLine(String text) {
		text = text.substring(text.indexOf("\"") + 1);
		String module = text.substring(0, text.indexOf("\""));
		text = text.substring(text.indexOf("\"") + 2);
		String action = text.substring(0, text.indexOf("("));
		
		ResponseOfParsing response = new ResponseOfParsing();
		response.setNameOfElement(action.replace(".", ""));
		response.setPackageOfElement(module);
		response.setTypeOfElement("module");
		
		return response;
	}

}
