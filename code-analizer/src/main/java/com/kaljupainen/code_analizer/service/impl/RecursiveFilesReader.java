package com.kaljupainen.code_analizer.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RecursiveFilesReader {
	public List<String> getAllFileNames(String path) {
		List<String> listOfFiles = new ArrayList<String>();
		try (Stream<Path> walk = Files.walk(Paths.get(path))) {

			listOfFiles = walk.filter(Files::isRegularFile)
					.map(x -> x.toString()).collect(Collectors.toList());			

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return listOfFiles;
	}
}
