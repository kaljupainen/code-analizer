package com.kaljupainen.code_analizer.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;

public class FileAnalizerConsumer {
		
	public void analize(String filePath) {
		try {
			Files.lines(Path.of(filePath)).forEach(line -> new LineAnalizer().accept(line));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
