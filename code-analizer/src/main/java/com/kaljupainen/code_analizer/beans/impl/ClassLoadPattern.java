package com.kaljupainen.code_analizer.beans.impl;

import com.kaljupainen.code_analizer.beans.PatternsOfCode;

public class ClassLoadPattern extends PatternsOfCode {

	@Override
	public boolean compare(String text) {
		if (text.toLowerCase().indexOf("class.load") > -1) {
			return true;						
		}
		else {
			return false;
		}
	}

}
