package com.kaljupainen.code_analizer;

import java.util.List;

import com.kaljupainen.code_analizer.service.impl.FileAnalizerConsumer;
import com.kaljupainen.code_analizer.service.impl.RecursiveFilesReader;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args )
    {
    	String path = "/home/kaljupainen/tmp/test_js/";
    	
    	RecursiveFilesReader reader = new RecursiveFilesReader();
    	List<?> listOfFiles = reader.getAllFileNames(path);
    	FileAnalizerConsumer analizer = new FileAnalizerConsumer();
    	for (Object filePathObject:listOfFiles) {
    		String filePath = (String)filePathObject;
    		analizer.analize(filePath);
    	}
    	
    }
}
